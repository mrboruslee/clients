import requests
import pytz
import cerberus

from datetime import datetime, timedelta

from django.conf import settings
from django.db import transaction
from rest_framework import serializers

from apps.core.serializers import BaseModelSerializer
from .models import Lead, LeadVisitedPage, Borrower, Person, Entrepreneur, Corporation, History

from apps.dicts import models as dicts


class BorrowerSerializer(BaseModelSerializer):

    class Meta:
        model = Borrower
        fields = '__all__'


class CorporationSerializer(BorrowerSerializer):

    class Meta:
        model = Corporation
        fields = '__all__'


class PersonSerializer(BorrowerSerializer):

    class Meta:
        model = Person
        fields = '__all__'


class EntrepreneurSerializer(BorrowerSerializer):

    class Meta:
        model = Entrepreneur
        fields = '__all__'


class LeadVisitedPageSerializer(serializers.ModelSerializer):

    class Meta:
        model = LeadVisitedPage
        fields = '__all__'


class LeadSerializer(BaseModelSerializer):
    visited_pages = LeadVisitedPageSerializer(many=True, read_only=True)

    class Meta:
        model = Lead
        fields = '__all__'


class PublicLeadSerializer(serializers.Serializer):

    visitor_id = serializers.CharField(write_only=True)
    data = serializers.JSONField(write_only=True)

    def create(self, validated_data):

        url = f"http://{settings.PIWIK_HOST}/index.php"
        params = {
            "module": "API",
            "action": "index",
            "visitorId": validated_data['visitor_id'],
            "idSite": 1,
            "period": "day",
            "method": "Live.getVisitorProfile",
            "format": "json",
            "expanded": "1",
            "token_auth": settings.PIWIK_TOKEN
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        data = resp.json()

        v = cerberus.Validator({
            'lastVisits': {'type': 'list', 'empty': False, 'schema': {
                'type': 'dict',
                'schema': {
                    'visitIp': {'type': 'string'},
                    'languageCode': {'type': 'string'},
                    'language': {'type': 'string'},
                    'city': {'type': 'string', 'nullable': True},
                    'country': {'type': 'string'},
                    'region': {'type': 'string', 'nullable': True},
                    'actionDetails': {'type': 'list', 'empty': False, 'schema': {
                        'type': 'dict',
                        'schema': {
                            'serverTimePretty': {'type': 'string'},
                            'timeSpent': {'type': 'string'},
                            'url': {'type': 'string'},
                            'pageTitle': {'type': 'string'},
                        }
                    }}
                }
            }}})
        v.allow_unknown = True
        if not v.validate(data):
            raise Exception(f"Piwik returns unexpected result: {v.errors}")

        with transaction.atomic():
            last_visit = data['lastVisits'][-1]

            ip_address = last_visit['visitIp']

            language_code = last_visit['languageCode']
            language_name = last_visit['language']
            language, _ = dicts.Language.objects.get_or_create(code=language_code, defaults={"name": language_name})

            city = last_visit['city']
            if city:
                city, _ = dicts.City.objects.get_or_create(name=city)

            region = f"{last_visit['country']} {last_visit['region']}"
            region, _ = dicts.Region.objects.get_or_create(name=region)

            lead = Lead.objects.create(
                is_manual=False,
                meta=validated_data['data'],
                language=language,
                region=region,
                city=city,
                ip_address=ip_address,
            )

            visits = []
            for visit in last_visit['actionDetails']:
                entry = datetime.strptime(visit['serverTimePretty'], '%b %d, %Y %H:%M:%S').replace(tzinfo=pytz.utc)
                leave = entry + timedelta(seconds=int(visit.get('timeSpent', '0')))
                visits.append(LeadVisitedPage(
                    lead=lead,
                    page=visit['url'],
                    title=visit['pageTitle'],
                    entry=entry,
                    leave=leave,
                ))
            LeadVisitedPage.objects.bulk_create(visits)
        return lead


class BorrowerHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = History
        exclude = ('id', 'object_id', 'content_type')
