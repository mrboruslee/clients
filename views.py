from django.utils.translation import ugettext_lazy as _
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.accounts.permissions import APIKeyPermission
from apps.accounts.utils import Path
from .models import Lead, Borrower, History
from .serializers import LeadSerializer, BorrowerSerializer, PersonSerializer, CorporationSerializer, \
    EntrepreneurSerializer, PublicLeadSerializer, BorrowerHistorySerializer


class LeadViewSet(viewsets.ModelViewSet):
    serializer_class = LeadSerializer
    queryset = Lead.objects.all()
    ui = Path(_('Leads'))


class PublicLeadViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):
    permission_classes = [APIKeyPermission, ]
    serializer_class = PublicLeadSerializer


class BorrowerViewSet(viewsets.ModelViewSet):
    serializer_class = BorrowerSerializer
    queryset = Borrower.objects.all()
    ui = Path(_('Borrowers'))

    MAPPING = {
        Borrower.PERSON: PersonSerializer,
        Borrower.CORPORATION: CorporationSerializer,
        Borrower.ENTREPRENEUR: EntrepreneurSerializer
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.client_type = None

    def initialize_request(self, request, *args, **kwargs):
        request = super().initialize_request(request, *args, **kwargs)
        if self.action in ('retrieve', 'partial_update', 'destroy'):
            self.client_type = Borrower.objects.get(id=self.kwargs[self.lookup_field]).type
        elif self.action == 'create':
            self.client_type = request.data.get('type')
        return request

    def get_queryset(self):
        qs = super().get_queryset()
        if self.action in ('retrieve', 'partial_update', 'destroy', 'create'):
            # switch client model to current type
            qs = self.MAPPING[self.client_type].Meta.model.objects.all()
        return qs

    def get_serializer_class(self):
        return self.MAPPING.get(self.client_type, self.serializer_class)

    @action(detail=True)
    def history(self, request, pk):
        qs = History.objects.filter(
            object_id=pk, content_type__model='borrower'
        ).order_by('-timestamp')
        return Response(data=BorrowerHistorySerializer(qs, many=True).data)
