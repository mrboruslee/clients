from django.urls import path, include
from rest_framework.routers import SimpleRouter

from . import views

router = SimpleRouter()
router.register('leads', views.LeadViewSet)
router.register('borrowers', views.BorrowerViewSet)

public_router = SimpleRouter()
public_router.register('leads', views.PublicLeadViewSet, base_name="public-lead")

urlpatterns = [
    path('', include(router.urls)),
    path('public/', include(public_router.urls)),
]
