from importlib import import_module

from django.apps import AppConfig


class ClientsConfig(AppConfig):
    name = 'apps.clients'

    def ready(self):
        import_module('apps.clients.signals')
