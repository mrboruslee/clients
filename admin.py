from django.contrib import admin

from .models import Lead, LeadVisitedPage, Person, Entrepreneur, Corporation, Contact, BankAccount


class LeadVisitedPageAdmin(admin.StackedInline):
    model = LeadVisitedPage
    extra = 0


class ContactAdmin(admin.StackedInline):
    model = Contact
    extra = 0


class BankAccountAdmin(admin.StackedInline):
    model = BankAccount
    extra = 0


@admin.register(Lead)
class LeadAdmin(admin.ModelAdmin):
    list_display = ('ip_address', 'region',)
    inlines = [LeadVisitedPageAdmin]


class BorrowerAdmin(admin.ModelAdmin):
    exclude = ('deleted_at',)
    inlines = (ContactAdmin, BankAccountAdmin)


@admin.register(Person)
class PersonAdmin(BorrowerAdmin):
    pass


@admin.register(Entrepreneur)
class EntrepreneurAdmin(BorrowerAdmin):
    pass


@admin.register(Corporation)
class CorporationAdmin(BorrowerAdmin):
    pass
