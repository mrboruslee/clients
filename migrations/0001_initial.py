import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('dicts', '0005_campaign_city_cta_product_region_source'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lead',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateTimeField(null=True)),
                ('ip_address', models.GenericIPAddressField(blank=True, null=True)),
                ('meta', django.contrib.postgres.fields.jsonb.JSONField(default={})),
                ('is_manual', models.BooleanField(default=False)),
                ('campaign', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='leads', to='dicts.Campaign')),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='leads', to='dicts.City')),
                ('cta', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='leads', to='dicts.CTA')),
                ('language', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='leads', to='dicts.Language')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='leads', to='dicts.Product')),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='leads', to='dicts.Region')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='leads', to='dicts.Source')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LeadVisitedPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('page', models.CharField(max_length=256)),
                ('title', models.CharField(max_length=256)),
                ('entry', models.DateTimeField()),
                ('leave', models.DateTimeField()),
                ('lead', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='visited_pages', to='clients.Lead')),
            ],
        ),
    ]
