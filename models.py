from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

from rest_framework.utils.encoders import JSONEncoder

from apps.core.models import DEFAULT_MAX_LENGTH, BaseTimestampableModel, BaseModel, AttrTrackerMixin


# abstract models used to construct Burrower subclasses

class PassportFields(models.Model):
    id_number = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    id_type = models.ForeignKey('dicts.IDType', on_delete=models.PROTECT)
    id_issued_by = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    id_issued_at = models.DateField()

    class Meta:
        abstract = True


class PersonFields(models.Model):
    GENDER_CHOICES = (
        ('M', _('Male')),
        ('F', _('Female'))
    )

    first_name = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    last_name = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    dob = models.DateField()  # date of birth
    gender = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=GENDER_CHOICES)
    ssn = models.CharField(max_length=DEFAULT_MAX_LENGTH)  # social security number

    class Meta:
        abstract = True


class EntrepreneurCorporationFields(models.Model):
    dor = models.DateField()  # date of registration
    corporate_id = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    field_of_activity = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    corporation_type = models.ForeignKey('dicts.CorporationType', on_delete=models.PROTECT)
    experience = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    sic = models.CharField(max_length=DEFAULT_MAX_LENGTH)  # Standard Industrial Classification
    tax_system = models.ForeignKey('dicts.TaxSystem', on_delete=models.PROTECT)
    commodities = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    commodities_market = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    average_monthly_income = models.FloatField()
    fluctuation = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    is_agent = models.BooleanField()
    is_license = models.BooleanField()
    license_expiry_date = models.DateField(blank=True, null=True)
    employees_count = models.PositiveIntegerField()
    is_trial = models.BooleanField()
    trial_description = models.TextField(blank=True)
    has_overdue_obligations = models.BooleanField()
    overdue_obligations_description = models.TextField(blank=True)

    class Meta:
        abstract = True


class EntrepreneurPersonFields(PassportFields, PersonFields):
    FAMILY_STATUS_CHOICES = (
        ('M', _('Married')),
        ('D', _('Divorced')),
        ('S', _('Single'))
    )

    education = models.ForeignKey('dicts.Education', on_delete=models.PROTECT)
    family_status = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=FAMILY_STATUS_CHOICES)
    citizenship = models.ForeignKey('dicts.Citizenship', on_delete=models.PROTECT)

    class Meta:
        abstract = True


class Borrower(BaseTimestampableModel, AttrTrackerMixin):
    PERSON = 'P'
    ENTREPRENEUR = 'E'
    CORPORATION = 'C'
    CLIENT_TYPE_CHOICES = (
        (PERSON, _('Person')),
        (ENTREPRENEUR, _('Entrepreneur')),
        (CORPORATION, _('Corporation'))
    )

    exclude_tracking_fields = ('updated_at', 'id', 'borrower_ptr')

    type = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=CLIENT_TYPE_CHOICES)
    pob = models.DateField()  # place of birth
    office = models.ForeignKey('accounts.Unit', on_delete=models.PROTECT, related_name='clients')
    language = models.ForeignKey('dicts.Language', on_delete=models.PROTECT, related_name='clients')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='clients')
    country = models.ForeignKey('accounts.Country', on_delete=models.PROTECT, related_name='clients')
    condition = models.ForeignKey('dicts.Condition', on_delete=models.PROTECT, related_name='clients')
    first_connection = models.ForeignKey('dicts.FirstConnection', on_delete=models.PROTECT)
    credit_score = models.FloatField()
    is_resident = models.BooleanField()
    tax_id = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    photo = models.ForeignKey('docs.DocumentFile', blank=True, null=True,
                              on_delete=models.PROTECT)
    job_start_date = models.DateField()
    residence_address = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    mailing_address = models.CharField(max_length=DEFAULT_MAX_LENGTH)


class Person(Borrower, EntrepreneurPersonFields):
    place_of_work = models.ForeignKey('dicts.PlaceOfWork', on_delete=models.PROTECT)
    job_position = models.ForeignKey('dicts.JobPosition', on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Entrepreneur(Borrower, EntrepreneurCorporationFields, EntrepreneurPersonFields):
    pass


class Corporation(Borrower, EntrepreneurCorporationFields):
    beneficiary = models.ForeignKey('CorporationOwner', related_name='beneficiaries',
                                    on_delete=models.SET_NULL, blank=True, null=True)


class History(BaseModel):
    # can be null if changes was made by the system
    user = models.ForeignKey('accounts.User', null=True, on_delete=models.PROTECT)

    # https://docs.djangoproject.com/en/2.0/ref/contrib/contenttypes/#generic-relations
    content_type = models.ForeignKey('contenttypes.ContentType', on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    action = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    timestamp = models.DateTimeField(auto_now_add=True)
    changes = JSONField(encoder=JSONEncoder)


class CorporationOwner(BaseModel, PersonFields, PassportFields):
    corporation = models.ForeignKey('Corporation', on_delete=models.CASCADE)
    share = models.FloatField()
    tax_id = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    pob = models.CharField(max_length=DEFAULT_MAX_LENGTH)  # place of birth
    address = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    is_public = models.BooleanField()
    is_beneficiary = models.BooleanField()


class ClientLabel(BaseModel):
    clients = models.ManyToManyField('Borrower', related_name='labels')

    name = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    description = models.TextField()
    text_color = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    back_color = models.CharField(max_length=DEFAULT_MAX_LENGTH)


class Contact(BaseModel, AttrTrackerMixin):
    TYPE_CHOICES = (
        ('email', _('email')),
        ('phone', _('phone')),
        ('skype', _('skype')),
        ('telegram', _('telegram'))
    )

    STATE_CHOICES = (
        ('active', _('active')),
        ('disabled', _('disabled')),
        ('verified', _('verified'))
    )

    exclude_tracking_fields = ('id', 'borrower')

    borrower = models.ForeignKey('Borrower', on_delete=models.CASCADE, related_name='contacts')

    type = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=TYPE_CHOICES)
    state = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=STATE_CHOICES)
    value = models.CharField(max_length=DEFAULT_MAX_LENGTH)

    def get_model_dict(self):
        return {self.type: f'{self.value} ({self.state})'}

    def get_verbose_difference(self, obj_deletion=False, obj_creation=False):
        changes = {'old': {}, 'new': {}}
        diff = self.get_difference()

        if obj_deletion or obj_creation:
            key = 'old' if obj_deletion else 'new'
            changes[key] = self.get_model_dict()

        elif diff:
            changes['old'] = {
                self._initials['type']: f"{self._initials['value']}"
                                        f" ({self._initials['state']})"
            }
            changes['new'] = self.get_model_dict()

        return changes

    def __str__(self):
        return f'{self.type}: {self.value} ({self.state})'


class Counterparty(BaseModel):
    DIRECTION_CHOICES = (
        ('Provider', _('Provider')),
        ('Buyer', _('Buyer'))
    )

    exclude_tracking_fields = ('id', 'borrower')

    borrower = models.ForeignKey('Borrower', on_delete=models.CASCADE)
    name = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    type = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=Borrower.CLIENT_TYPE_CHOICES)
    direction = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=DIRECTION_CHOICES)
    share = models.FloatField()


class BankAccount(BaseModel, AttrTrackerMixin):
    exclude_tracking_fields = ('id', 'borrower')

    borrower = models.ForeignKey('Borrower', on_delete=models.CASCADE)
    bank = models.ForeignKey('dicts.Bank', on_delete=models.PROTECT)
    account_number = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    iban = models.CharField(max_length=DEFAULT_MAX_LENGTH)  # International Bank Account Number
    details = models.TextField()


class ExternalCredit(BaseModel, AttrTrackerMixin):
    exclude_tracking_fields = ('id', 'borrower')

    borrower = models.ForeignKey('Borrower', on_delete=models.CASCADE)
    bank = models.ForeignKey('dicts.Bank', on_delete=models.PROTECT)
    sum = models.FloatField()
    currency = models.CharField(max_length=DEFAULT_MAX_LENGTH)  # FIXME: should it be dict?
    start_date = models.DateField()
    end_date = models.DateField()


class MortgagedProperty(BaseModel):
    application = models.ForeignKey('applications.Application', on_delete=models.CASCADE)
    client = models.ForeignKey('Borrower', on_delete=models.CASCADE)
    type = models.ForeignKey('dicts.PropertyType', on_delete=models.PROTECT)
    ownership_type = models.ForeignKey('dicts.OwnershipType', on_delete=models.PROTECT)
    square = models.FloatField()
    address = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    owner_name = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    brand = models.CharField(max_length=DEFAULT_MAX_LENGTH, blank=True)
    model = models.CharField(max_length=DEFAULT_MAX_LENGTH, blank=True)
    year = models.PositiveIntegerField(null=True, blank=True)


class Guarantor(BaseModel, PassportFields):
    application = models.ForeignKey('applications.Application', on_delete=models.CASCADE)
    client = models.ForeignKey('Borrower', on_delete=models.CASCADE)
    type = models.CharField(max_length=DEFAULT_MAX_LENGTH, choices=Borrower.CLIENT_TYPE_CHOICES)
    name = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    corporate_id = models.CharField(max_length=DEFAULT_MAX_LENGTH)


def default_lead_meta():
    return {}


class Lead(BaseTimestampableModel):
    borrower = models.ForeignKey('Borrower', null=True, blank=True, on_delete=models.CASCADE)

    ip_address = models.GenericIPAddressField(blank=True, null=True)

    language = models.ForeignKey('dicts.Language', on_delete=models.PROTECT, related_name='leads')
    region = models.ForeignKey('dicts.Region', on_delete=models.PROTECT, related_name='leads')

    city = models.ForeignKey('dicts.City', on_delete=models.PROTECT, related_name='leads', null=True)

    # Source of the lead: referral site or other source lead came from
    source = models.ForeignKey('dicts.Source', on_delete=models.PROTECT, related_name='leads', null=True)
    product = models.ForeignKey('dicts.Product', on_delete=models.PROTECT, related_name='leads', null=True)
    campaign = models.ForeignKey('dicts.Campaign', on_delete=models.PROTECT, related_name='leads', null=True)
    # call to action (buttons, etc)
    cta = models.ForeignKey('dicts.CTA', on_delete=models.PROTECT, related_name='leads', null=True)

    meta = JSONField(default=default_lead_meta)
    is_manual = models.BooleanField(default=False)

    @classmethod
    def unassigned_leads_pool(cls):
        return cls.objects.filter(Q(client__isnull=True) | Q(client__owner__isnull=True))


class LeadVisitedPage(models.Model):
    # This model can be useless (and removed) after integrating with GA
    lead = models.ForeignKey('Lead', on_delete=models.CASCADE, related_name='visited_pages')
    page = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    title = models.CharField(max_length=DEFAULT_MAX_LENGTH)
    entry = models.DateTimeField()
    leave = models.DateTimeField()
