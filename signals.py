from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from apps.core import local
from .models import History, Person, Corporation, Entrepreneur, \
    Counterparty, BankAccount, ExternalCredit, Contact, Borrower
from apps.applications.models import Application


@receiver(post_save, sender=Counterparty)
@receiver(post_save, sender=BankAccount)
@receiver(post_save, sender=ExternalCredit)
@receiver(post_save, sender=Contact)
@receiver(post_save, sender=Entrepreneur)
@receiver(post_save, sender=Person)
@receiver(post_save, sender=Corporation)
def borrower_history(sender, instance,
                     created: bool, **kwargs):
    diff = instance.get_verbose_difference(obj_creation=created)
    if diff['new']:
        action = 'Create' if created else 'Edit'
        if not any(isinstance(instance, cls) for cls in Borrower.__subclasses__()):
            # if it's borrower's related entity
            action = f'{action} {sender._meta.verbose_name.title()}'
            content_object_id = instance.borrower_id
        else:
            # if it's borrower subclass
            content_object_id = instance.borrower_ptr_id

        History.objects.create(
            changes=diff, object_id=content_object_id,
            content_type=ContentType.objects.get_for_model(Borrower),
            action=action,
            user_id=getattr(local, 'user_id', None)
        )


# TODO: Processing, sales models are also a part of application history
# So we should define them here as soon as they will be implemented.
@receiver(post_save, sender=Application)
def application_history(sender, instance, created: bool, **kwargs):
    diff = instance.get_verbose_difference(obj_creation=created)
    if diff['new']:
        History.objects.create(
            changes=diff,
            content_object=instance,
            action='Create' if created else 'Edit',
            user_id=getattr(local, 'user_id', None)
        )


@receiver(pre_delete, sender=Counterparty)
@receiver(pre_delete, sender=BankAccount)
@receiver(pre_delete, sender=ExternalCredit)
@receiver(pre_delete, sender=Contact)
def delete_reference(sender, instance, **kwargs):
    History.objects.create(
        changes=instance.get_verbose_difference(obj_deletion=True),
        object_id=instance.borrower_id,
        content_type=ContentType.objects.get_for_model(Borrower),
        action=f'Deleted {sender._meta.verbose_name.title()}',
        user_id=getattr(local, 'user_id', None)
    )
