from datetime import datetime

import mock
import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.accounts.models import Unit, Country
from apps.clients.models import Lead, Borrower, Entrepreneur, Person, Corporation
from apps.clients.views import BorrowerViewSet
from apps.dicts.models import Language, Condition, FirstConnection, IDType, \
    Education, Citizenship, PlaceOfWork, JobPosition, CorporationType, TaxSystem


@pytest.fixture
def base_borrower_data(borrowers_dicts, user, units) -> dict:
    return {
        'pob': str(datetime.now().date()),
        'office': Unit.objects.last().id,
        'language': Language.objects.last().code,
        'condition': Condition.objects.last().id,
        'first_connection': FirstConnection.objects.last().id,
        'credit_score': 1,
        'is_resident': False,
        'tax_id': 'some-id',
        'job_start_date': str(datetime.now().date()),
        'residence_address': 'address',
        'mailing_address': 'address',
        'country': Country.objects.last().id,
        'owner': user.id
    }


@pytest.fixture
def person_borrower_data(base_borrower_data) -> dict:
    data = base_borrower_data.copy()
    data.update({
        'type': Borrower.PERSON,
        'id_number': 'id-number',
        'id_type': IDType.objects.last().id,
        'id_issued_by': 'someone',
        'id_issued_at': str(datetime.now().date()),
        'first_name': 'person',
        'last_name': 'person',
        'dob': str(datetime.now().date()),
        'gender': 'M',
        'ssn': 'some number',
        'education': Education.objects.last().id,
        'family_status': 'M',
        'citizenship': Citizenship.objects.last().id,
        'place_of_work': PlaceOfWork.objects.last().id,
        'job_position': JobPosition.objects.last().id
    })
    return data


@pytest.fixture
def corporation_borrower_data(base_borrower_data) -> dict:
    data = base_borrower_data.copy()
    data.update({
        'type': Borrower.CORPORATION,
        'dor': str(datetime.now().date()),
        'corporate_id': 'ID',
        'field_of_activity': 'some activity',
        'corporation_type': CorporationType.objects.last().id,
        'experience': 'huge',
        'sic': 'sic',
        'tax_system': TaxSystem.objects.last().id,
        'commodities': 'commodities',
        'commodities_market': 'commodities market',
        'average_monthly_income': 4000.1,
        'fluctuation': 'well',
        'is_agent': True,
        'is_license': False,
        'employees_count': 2,
        'is_trial': False,
        'has_overdue_obligations': False,
    })
    return data


@pytest.fixture
def entrepreneur_borrower_data(corporation_borrower_data, person_borrower_data) -> dict:
    data = corporation_borrower_data.copy()
    data.update(person_borrower_data)
    data.update({
        'type': Borrower.ENTREPRENEUR
    })
    return data


@pytest.fixture
def lead_data(campaigns, cities, cta, products, regions, sources, languages):
    return {
        'ip_address': '8.8.8.8',
        'is_manual': True,
        'language': languages.first().code,
        'cta': cta.first().id,
        'product': products.first().id,
        'region': regions.first().id,
        'source': sources.first().id,
        'city': cities.first().id,
        'campaign': campaigns.first().id
    }


@pytest.fixture
def public_lead_data():
    return {
        "visitor_id": "test",
        "data": {
        }
    }


@pytest.fixture
def stats_resp():
    return {'lastVisits': [{
        'visitIp': "127.0.0.1",
        'languageCode': "en",
        'language': "English",
        'city': "Lobdon",
        'country': "GB",
        'region': "",
        'actionDetails': [{
            'serverTimePretty': "Aug 1, 2018 10:34:50",
            'timeSpent': "2",
            'url': "http://localhost:8001/landing_test.html",
            'pageTitle': "Landing page test",
        }]
    }]}


def test_create_lead(lead_data, authorized_api_client):
    response = authorized_api_client.post(reverse('lead-list'), data=lead_data, format='json')
    assert response.status_code == status.HTTP_201_CREATED


def test_get_lead(lead: Lead, authorized_api_client: APIClient):
    response = authorized_api_client.get(reverse('lead-list'))
    data = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert data['total'] == Lead.objects.count()


@mock.patch("apps.clients.serializers.requests")
def test_api_key_permissions(mock_requests, public_lead_data, apikey_authorized_api_client, stats_resp):
    resp = mock.Mock()
    resp.json.return_value = stats_resp
    mock_requests.get.return_value = resp
    response = apikey_authorized_api_client.post(reverse('public-lead-list'), data=public_lead_data, format='json')
    assert response.status_code == status.HTTP_201_CREATED


def test_create_borrower(entrepreneur_borrower_data, corporation_borrower_data,
                         person_borrower_data, authorized_api_client: APIClient):
    for data in [entrepreneur_borrower_data, corporation_borrower_data, person_borrower_data]:
        response = authorized_api_client.post(reverse('borrower-list'), data=data)
        assert response.status_code == status.HTTP_201_CREATED

    assert Entrepreneur.objects.count() == 1
    assert Person.objects.count() == 1
    assert Corporation.objects.count() == 1


def test_get_borrowers_list(borrowers, authorized_api_client: APIClient):
    response = authorized_api_client.get(reverse('borrower-list'))
    assert response.status_code == status.HTTP_200_OK
    data = response.json()
    assert data['total'] == Borrower.objects.count()


def test_get_borrowers(borrowers, authorized_api_client: APIClient):
    for b in borrowers:
        response = authorized_api_client.get(reverse('borrower-detail', args=[b.id]))
        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        serializer_class = BorrowerViewSet.MAPPING[data['type']['value']]
        assert serializer_class().fields.keys() == data.keys()


def test_update_borrower(borrowers, user, authorized_api_client: APIClient):
    b = borrowers.last()
    addr = 'new address'
    response = authorized_api_client.patch(reverse('borrower-detail', args=[b.id]), data={
        'residence_address': addr, 'mailing_address': addr
    })
    assert response.status_code == status.HTTP_200_OK

    response = authorized_api_client.get(reverse('borrower-history', args=[b.id]))
    assert response.status_code == status.HTTP_200_OK

    data = response.json()
    history_item = data[0]
    assert history_item['changes'] == {
        'old': {'mailing address': b.mailing_address, 'residence address': b.residence_address},
        'new': {'mailing address': addr, 'residence address': addr}
    }

    assert history_item['user'] == user.id


def test_delete_borrower(borrowers, authorized_api_client: APIClient):
    for b in borrowers:
        response = authorized_api_client.delete(reverse('borrower-detail', args=[b.id]))
        assert response.status_code == status.HTTP_204_NO_CONTENT
