import pytest
from django.core.management import call_command

from apps.clients.models import Lead, Borrower


@pytest.fixture
def lead(campaigns, cities, cta, products,
         regions, sources, languages):
    call_command('loaddata', 'lead')
    return Lead.objects.get(id=1)


@pytest.fixture
def borrowers(user, units):
    call_command('loaddata', 'borrowers-dicts', 'borrowers')
    return Borrower.objects.all()
